Required Python Libraries:

1. Numpy (numpy)
2. Pandas (pandas)
3. Keras (keras)
4. Scikit Learn (sklearn)
5. Matplot Library (matplotlib)
6. Live Loss Plot (livelossplot)

You can download all of them using "pip install"